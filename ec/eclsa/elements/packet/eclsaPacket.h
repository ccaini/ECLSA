/*
eclsaPacketManager.h

 Author: Nicola Alessi (nicola.alessi@studio.unibo.it)
 Co-author of HSLTP extensions: Azzurra Ciliberti (azzurra.ciliberti@studio.unibo.it)
 Project Supervisor: Carlo Caini (carlo.caini@unibo.it)

Copyright (c) 2016, Alma Mater Studiorum, University of Bologna
 All rights reserved.

 * */

#ifndef _ECPACKETMANAGER_H_
#define _ECPACKETMANAGER_H_
#include "../matrix/eclsaMatrix.h"
#include <stdbool.h>


/*Header */
typedef struct
{
	uint8_t 	version;		//1Byte
	uint8_t 	extensionCount;	//1Byte
	uint8_t 	flags;
	uint16_t 	engineID; 		//2Byte
	uint16_t 	matrixID;		//BID,Matrix ID, 2Byte
	uint16_t 	symbolID;		//PID, 2Byte
	uint16_t 	segmentsAdded;	//number of added segments, 2Byte
	uint16_t 	K;
	uint16_t 	N;
	uint16_t 	T; 				//2Byte
} EclsaHeader;

#define ECLSA_HEADER_LENGTH 17

typedef enum
{
	FEEDBACK_REQUEST_MASK 	= 1,
	CONTINUOUS_MODE_MASK 	= 2,
	HSLTP_MODE_MASK 		= 4
} HeaderFlags; //2^n , n= 0,1,2, ...


/*Eclsa packet functions*/
void createEclsaHeader(EclsaMatrix *matrix,EclsaHeader *header);
void createEclsaPacket(EclsaMatrix *matrix, EclsaHeader *header, int symbolID, char *buffer,int *bufferLength);
bool isUncodedEclsaPacket(EclsaHeader *header);
bool parseEclsaIncomingPacket(EclsaHeader *outHeader, FecElement **outEncodingCode,char *inBuffer, int inBufferLength, char **outBuffer, int *outBufferLength, int maxT);



#endif
