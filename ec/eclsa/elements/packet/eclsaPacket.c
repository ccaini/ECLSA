/*
eclsaPacketManager.c

 Author: Nicola Alessi (nicola.alessi@studio.unibo.it)
 Co-author of HSLTP extensions: Azzurra Ciliberti (azzurra.ciliberti@studio.unibo.it)
 Project Supervisor: Carlo Caini (carlo.caini@unibo.it)

Copyright (c) 2016, Alma Mater Studiorum, University of Bologna
 All rights reserved.

 * */
#include "eclsaPacket.h"
#include "../sys/eclsaLogger.h"
#include "../../adapters/codec/eclsaCodecAdapter.h"

#include <string.h>
#include <netinet/in.h> //AG: htons, ntohs

//CCaini
bool isInfoEclsaPacket(EclsaHeader *header);

/*Eclsa packet functions*/
void createEclsaHeader(EclsaMatrix *matrix,EclsaHeader *header)
{
	memset(header, 0, sizeof(EclsaHeader));

	header->version = 0;

	//todo extensions must be parsed
	header->extensionCount = 0;
	
	if(matrix->HSLTPModeEnabled)
		header->flags = header->flags | HSLTP_MODE_MASK;

	if(matrix->feedbackEnabled)
		header->flags = header->flags | FEEDBACK_REQUEST_MASK;
	
	header->engineID = htons(matrix->engineID);

	header->matrixID = htons(matrix->ID);

	//Create the eclsa header with PID=0 (once per matrix)
	//The actual PID will be set later by the createEclsaPacket function.
	header->symbolID = 0;

	header->segmentsAdded = htons(matrix->infoSegmentAddedCount);
	
	if(matrix->encodingCode != NULL)
		{
		if(matrix->encodingCode->continuous)
			header->flags =	header->flags | CONTINUOUS_MODE_MASK;

		//header->codeID= matrix->encodingCode->ID;

		header->K = htons(matrix->encodingCode->K);
		header->N = htons(matrix->encodingCode->N);
		header->T = htons(matrix->encodingCode->T);
		}
}
void createEclsaPacket(EclsaMatrix *matrix, EclsaHeader *header, int symbolID, char * buffer,int *bufferLength)
{
	uint8_t headerOffset = 0;
	int i;
	int T;
	uint16_t segmentLength, htons_segmentLength; //2Byte
	char *matrixSegment = getSymbolFromCodecMatrix(matrix->abstractCodecMatrix, symbolID);
	char *ltpSegment;
	int universalCodecStatus = convertToAbstractCodecStatus(matrix->codecStatus);

	//The eclsa header has been created once with packetID=0 by createEclsaHeader();
	header->symbolID = htons(symbolID); // set the symbolID to the actual value

	//copy the eclsa header into the buffer
	// memcpy(buffer, header, sizeof(EclsaHeader));
	// AG: in order to avoid including a padding byte that could be present after the flags' byte,
	// the ECLSA header will be copied one element at a time
	memcpy(buffer + headerOffset, &(header->version), sizeof(header->version));
	headerOffset += sizeof(header->version);

	memcpy(buffer + headerOffset, &(header->extensionCount), sizeof(header->extensionCount));
	headerOffset += sizeof(header->extensionCount);

	memcpy(buffer + headerOffset, &(header->flags), sizeof(header->flags));
	headerOffset += sizeof(header->flags);

	memcpy(buffer + headerOffset, &(header->engineID), sizeof(header->engineID));
	headerOffset += sizeof(header->engineID);

	memcpy(buffer + headerOffset, &(header->matrixID), sizeof(header->matrixID));
	headerOffset += sizeof(header->matrixID);

	memcpy(buffer + headerOffset, &(header->symbolID), sizeof(header->symbolID));
	headerOffset += sizeof(header->symbolID);

	memcpy(buffer + headerOffset, &(header->segmentsAdded), sizeof(header->segmentsAdded));
	headerOffset += sizeof(header->segmentsAdded);

	memcpy(buffer + headerOffset, &(header->K), sizeof(header->K));
	headerOffset += sizeof(header->K);

	memcpy(buffer + headerOffset, &(header->N), sizeof(header->N));
	headerOffset += sizeof(header->N);

	memcpy(buffer + headerOffset, &(header->T), sizeof(header->T));
	headerOffset += sizeof(header->T);

	//if(matrix->encoded && !isInfoSegment(matrix,packetID) )
	if(universalCodecStatus == STATUS_CODEC_SUCCESS  && !isInfoSymbol(matrix, symbolID) )
		{
		//The eclsa payload must be filled with a matrix  redundancy segment, all the segment must be copied;
		T = matrix->encodingCode->T;
		//add the eclsa payload to the buffer
		memcpy(buffer + headerOffset, matrixSegment, sizeof(uint8_t) * T);
		*bufferLength = headerOffset + sizeof(uint8_t) * T ;

		//Optimization: eclsa packets often end with a long run of zeroes.
		//This often happens when the encoding matrix is not full (I<<K).
		//These zeroes can be safely erased to save bandwidth.

		for (i = *bufferLength - 1; i >= 0; i--)
			if (buffer[i] != '\0')
				{
				*bufferLength = i + 1;
				break;
				}

		}
	else
		{
		//The eclsa payload must be filled with a matrix uncoded or info segment,
		//only the actual data must be copied (drop the row padding);

		//extract the LTP (or another upper protocol) segment length from the first 2 bytes of ADT[packetID]
		memcpy(&segmentLength, matrixSegment, sizeof(uint16_t));
		
		//add the LTP segment length to the buffer
		htons_segmentLength = htons(segmentLength);
		memcpy(buffer + headerOffset, &htons_segmentLength, sizeof(uint16_t));

		//add the remaining part of the eclsa payload to the buffer
		ltpSegment = matrixSegment + sizeof(uint16_t);
		memcpy(buffer + headerOffset + sizeof(uint16_t), ltpSegment, segmentLength);
		*bufferLength = headerOffset + sizeof(uint16_t) + (int)segmentLength;
		}
}
bool parseEclsaIncomingPacket(EclsaHeader *outHeader, FecElement **outEncodingCode, char *inBuffer, int inBufferLength, char **outBuffer, int *outBufferLength, int maxT)
{
	uint8_t headerOffset = 0;
	uint16_t tmpSegmentLength; //2Byte
	bool continuousModeRequested = false;

	if(inBufferLength < ECLSA_HEADER_LENGTH)
		{
		debugPrint("Eclsa Header validation failed. Discarding segment. ( bufferLength < sizeof(EclsaHeader))");
		return false;
		}
	
	// memcpy(outHeader, inBuffer, sizeof(EclsaHeader));
	// AG: Since there could be a padding byte after the flags' byte in the EclsaHeader struct,
	// each element will be parsed separately
	memcpy(&(outHeader->version), inBuffer + headerOffset, sizeof(outHeader->version));
	headerOffset += sizeof(outHeader->version);

	memcpy(&(outHeader->extensionCount), inBuffer + headerOffset, sizeof(outHeader->extensionCount));
	headerOffset += sizeof(outHeader->extensionCount);

	memcpy(&(outHeader->flags), inBuffer + headerOffset, sizeof(outHeader->flags));
	headerOffset += sizeof(outHeader->flags);

	memcpy(&(outHeader->engineID), inBuffer + headerOffset, sizeof(outHeader->engineID));
	headerOffset += sizeof(outHeader->engineID);
	outHeader->engineID = ntohs(outHeader->engineID);

	memcpy(&(outHeader->matrixID), inBuffer + headerOffset, sizeof(outHeader->matrixID));
	headerOffset += sizeof(outHeader->matrixID);
	outHeader->matrixID = ntohs(outHeader->matrixID);

	memcpy(&(outHeader->symbolID), inBuffer + headerOffset, sizeof(outHeader->symbolID));
	headerOffset += sizeof(outHeader->symbolID);
	outHeader->symbolID = ntohs(outHeader->symbolID);

	memcpy(&(outHeader->segmentsAdded), inBuffer + headerOffset, sizeof(outHeader->segmentsAdded));
	headerOffset += sizeof(outHeader->segmentsAdded);
	outHeader->segmentsAdded = ntohs(outHeader->segmentsAdded);

	memcpy(&(outHeader->K), inBuffer + headerOffset, sizeof(outHeader->K));
	headerOffset += sizeof(outHeader->K);
	outHeader->K = ntohs(outHeader->K);

	memcpy(&(outHeader->N), inBuffer + headerOffset, sizeof(outHeader->N));
	headerOffset += sizeof(outHeader->N);
	outHeader->N = ntohs(outHeader->N);

	memcpy(&(outHeader->T), inBuffer + headerOffset, sizeof(outHeader->T));
	headerOffset += sizeof(outHeader->T);
	outHeader->T = ntohs(outHeader->T);

	if(outHeader->version != 0)
		{
		debugPrint("Eclsa Header validation failed. Discarding segment. (header->version != 0)");
		return false;
		}
	if(outHeader->segmentsAdded == 0)
		{
		debugPrint("Eclsa Header validation failed. Discarding segment. (header->segmentsAdded == 0)");
		return false;
		}
	//todo
	//if(header->T == 0)
		//{
		//debugPrint("WARNING: header->T == 0");
		//return false;
		//}
	if(outHeader->T > maxT)
		{
		debugPrint("Eclsa Header validation failed. Discarding segment. (header->T > maxT)");
		return false;
		}

	//remove the header from the buffer
	*outBuffer = inBuffer + headerOffset;
	*outBufferLength = inBufferLength - headerOffset;

	if(isUncodedEclsaPacket(outHeader))
		{
		memcpy(&tmpSegmentLength, *outBuffer, sizeof(uint16_t));
		*outBuffer = *outBuffer + sizeof(uint16_t);
		*outBufferLength = ntohs(tmpSegmentLength);
		return true;
		}
	else if(isInfoEclsaPacket(outHeader))
			{
			memcpy(&tmpSegmentLength, *outBuffer, sizeof(uint16_t));
			tmpSegmentLength = ntohs(tmpSegmentLength);
			memcpy(*outBuffer, &tmpSegmentLength, sizeof(uint16_t));
			}

	continuousModeRequested = (outHeader->flags & CONTINUOUS_MODE_MASK) != 0;
	*outEncodingCode = getFecElement(outHeader->K, outHeader->N, continuousModeRequested);

	if(*outEncodingCode == NULL)
		{
		//pointer not found (the wanted code is not in the code array)
		//todo togliendo questo commento si ottiene questa informazione che è utile.
		//essendo mostrata una volta per pacchetto, riempie il log se i segmenti sono tanti
		//debugPrint("Encoding code not found. Discarding segment.");
		return false;
		}
	return true;
}

bool isUncodedEclsaPacket(EclsaHeader *header)
	{
	return header->K == 0 && header->N == 0;
	}

//CCaini
bool isInfoEclsaPacket(EclsaHeader *header)
	{
	return header->symbolID < header->K;
	}

