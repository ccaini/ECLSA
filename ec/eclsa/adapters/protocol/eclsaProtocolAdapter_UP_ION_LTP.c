/*
eclsaProtocolAdapter_UP_ION_LTP.c

 Author: Nicola Alessi (nicola.alessi@studio.unibo.it)
 Co-author of HSLTP extensions: Azzurra Ciliberti (azzurra.ciliberti@studio.unibo.it)
 Project Supervisor: Carlo Caini (carlo.caini@unibo.it)

Copyright (c) 2016, Alma Mater Studiorum, University of Bologna
 All rights reserved.

 * */

/*
todo: ifdef LW_UDP
	  ifdef UL_ION_LTP
 */
#include <netdb.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <netinet/ip_icmp.h>
#include <netinet/udp.h>
#include <stdlib.h>
#include "ltpP.h"
#include "../../network_utils.h"
#include "eclsaProtocolAdapters.h"
#include "../../eclso.h"
#include "../../elements/sys/eclsaLogger.h"

#define LTP_VERSION	0;

typedef struct
{
	LtpVspan	*vspan;
} IonLtpEclsoEnvironment;

IonLtpEclsoEnvironment ltpEclsoEnv;

void initEclsoUpperLevel(int argc, char *argv[], EclsoEnvironment *env)
{
	/* ION declarations*/
	Sdr					sdr;
	LtpDB				*ltpDb;
	PsmAddress			vspanElt;
	unsigned long 		remoteEngineId; //to find span
	char				ownHostName[MAXHOSTNAMELEN];

	remoteEngineId = strtoul(argv[8], NULL, 0);

	if (remoteEngineId == 0)
	{
		printf("remoteEngineId %lu", remoteEngineId);
		PUTS("Usage: eclso {<remote engine's host name> | @}[:\
			<its port number>] <txbps (0=unlimited)> <remote engine ID>");
		exit(1);
	}
	if (ltpInit(0) < 0)
	{
		putErrmsg("eclso can't initialize LTP.", NULL);
		exit(1);
	}
	sdr = getIonsdr();

	if (!(sdr_begin_xn(sdr)))
	{
		putErrmsg("failed to begin sdr", NULL);
		exit(1);
	}
	findSpan(remoteEngineId, &ltpEclsoEnv.vspan, &vspanElt);
	if (vspanElt == 0)
	{
		sdr_exit_xn(sdr);
		putErrmsg("No such engine in database.", itoa(remoteEngineId));
		exit(1);
	}
	if (ltpEclsoEnv.vspan->lsoPid != ERROR && ltpEclsoEnv.vspan->lsoPid != sm_TaskIdSelf())
	{
		sdr_exit_xn(sdr);
		putErrmsg("LSO task is already started for this span.",itoa(ltpEclsoEnv.vspan->lsoPid));
		exit(1);
	}
	sdr_exit_xn(sdr);
	ltpDb= getLtpConstants();

	//Note that the first addendum is the max LTP segment size, LTP header included
	env->T=ltpEclsoEnv.vspan->maxXmitSegSize+ sizeof(uint16_t); //2B added for segmentLength
	env->ownEngineId=(unsigned short)ltpDb->ownEngineId;

	ionNoteMainThread("eclso");

	//Getting info for lower layer
	getNameOfHost(ownHostName, MAXHOSTNAMELEN);
}
void initEclsiUpperLevel(int argc, char *argv[], EclsiEnvironment *env)
{
	PsmPartition ltpwm = getIonwm();
	LtpVdb *vdb;

	if (ltpInit(0) < 0)
	{
		putErrmsg("eclsi can't initialize LTP.", NULL);
		exit(1);
	}

	vdb = getLtpVdb();
	for (PsmAddress elt = sm_list_first(ltpwm, vdb->seats); elt; elt = sm_list_next(ltpwm, elt))
	{
		LtpVseat *vseat = (LtpVseat *) psp(ltpwm, sm_list_data(ltpwm, elt));
		if (vseat != NULL && vseat->lsiPid != sm_TaskIdSelf())
		{
			putErrmsg("LSI task is already started.", itoa(vseat->lsiPid));
			exit(1);
		}
	}

	ionNoteMainThread("eclsi");
}

void sendSegmentToUpperProtocol		(char *buffer,int *bufferLength)
{
	if (ltpHandleInboundSegment(buffer, *bufferLength) < 0)
	{
		putErrmsg("Can't handle inbound segment.", NULL);
		exit(1);
	}
	//todo wait 0.5ms. this is a fix to avoid segments dropping in the interface with the
	//upper protocol. The origin of the dropping has not been thoroughly investigated yet.
	usleep(500);
}

void receiveSegmentFromUpperProtocol	(char *buffer,int *bufferLength)
{
	char *bufPtr;
	*bufferLength=ltpDequeueOutboundSegment(ltpEclsoEnv.vspan,&bufPtr);
	if(*bufferLength <= 0)
	{
		debugPrint("Can't acquire segment from LTP");
	}
	memcpy(buffer,bufPtr,*bufferLength);
}

