/*
eclsaProtocolAdapter_LP_UDP.c

 Author: Nicola Alessi (nicola.alessi@studio.unibo.it)
 Project Supervisor: Carlo Caini (carlo.caini@unibo.it)

Copyright (c) 2016, Alma Mater Studiorum, University of Bologna
 All rights reserved.

 * */

/*
todo: ifdef LW_UDP
	  ifdef UL_ION_LTP
 */

#include "eclsaProtocolAdapters.h"
#include "../../elements/sys/eclsaLogger.h"
#include "../../network_utils.h"
#include <time.h> //added by LBA as a test
//CCaini e poi lasciato in giro?
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <netinet/ip_icmp.h>
#include <netinet/udp.h>
#include <stdlib.h>

int reUseAddress(int fd) { //added by LBA to eliminate an ION dependency
#ifdef REUSEADDR_UNAVBL
	return 0;
#else
	int     result;
	int     i = 1;

	result = setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *) &i,
			sizeof i);
	if (result < 0)
	{
		//putSysErrmsg("can't make socket's address reusable", NULL);
	}

	return result;
#endif
}

static unsigned long getTimestamp();

static unsigned long startTimestamp = 0;

/* UDP */
typedef struct
{
	int 				socketDescriptor;
	INET_struct	    address;
	float               sleepSecPerBit; //eclso only
} UdpEnvironment;

UdpEnvironment udpEnv;

void initEclsoLowerLevel(int argc, char *argv[], EclsoEnvironment *env)
{
	//this function initialize UDP for eclso
	int returned_value = parseHostnameAndPort(argv[1], &(env->address), DEF_ECLSA_PORT_NUMBER);
	if (returned_value == -1) {
		exit(1); //The IP number MUST be set
	}

//udpEnv declared global
	udpEnv.sleepSecPerBit = 1.0 / env->txbps;
	udpEnv.address = env->address;
	udpEnv.socketDescriptor = socket(udpEnv.address.ai_family, SOCK_DGRAM, IPPROTO_UDP);
	if (udpEnv.socketDescriptor < 0)
	{
		debugPrint("LSO can't open UDP socket");
		exit(1);
	}
	//CCaini la funzione è asimmetrica rispetto alla sorella per eclsi
	//Manca la parte finale
}
void initEclsiLowerLevel(int argc, char *argv[], EclsiEnvironment *env)
{
	//this function initialize UDP for eclsi
	socklen_t			nameLength;

	int returned_value = parseHostnameAndPort(argv[1], &(env->address), DEF_ECLSA_PORT_NUMBER);
	if (returned_value == -1) {
		exit(1); //The IP number MUST be set
	}

	udpEnv.address = env->address;
	udpEnv.socketDescriptor = socket(udpEnv.address.ai_family, SOCK_DGRAM, IPPROTO_UDP);

	if (udpEnv.socketDescriptor < 0)
	{
		debugPrint("LSI can't open UDP socket");
		exit(1);
	}

	//CCaini con ogni probabilità da qui in poi si può riscrivere facendo a meno
	//dell'address pointer
	//questa parte manca nella gemella per eclso sopra

	struct sockaddr* address_pointer = NULL;
	switch (udpEnv.address.ai_family) {
	case AF_INET:
		address_pointer = (struct sockaddr*) &(udpEnv.address.sock_address.ipv4);
		break;

	case AF_INET6:
		address_pointer = (struct sockaddr*) &(udpEnv.address.sock_address.ipv6);
		break;
	}

	nameLength = sizeof(struct sockaddr);
	if (reUseAddress(udpEnv.socketDescriptor)
			|| bind(udpEnv.socketDescriptor, address_pointer, nameLength) < 0
			|| getsockname(udpEnv.socketDescriptor, address_pointer, &nameLength) < 0)
	{
		//closesocket(udpEnv.socketDescriptor);
		close(udpEnv.socketDescriptor);
		//putSysErrmsg("Can't initialize socket", NULL);
		debugPrint("Can't initialize socket");
		exit(1);
	}
}
void sendPacketToLowerProtocol	(char *buffer, int *bufferLength, void *udpProtocolData)
{
	int					bytesSent;
	float				sleep_secs;
	unsigned int		usecs = 0;

	struct sockaddr * workingSockAddr;
	if(udpProtocolData!=NULL) {
		workingSockAddr = udpProtocolData;
	}
	else {
		// Added by ALB
		if(udpEnv.address.ai_family == AF_INET) {
			workingSockAddr = (struct sockaddr *) &(udpEnv.address.sock_address.ipv4);
		}
		else if(udpEnv.address.ai_family == AF_INET6) {
			workingSockAddr = (struct sockaddr *) &(udpEnv.address.sock_address.ipv6);
		}
		else
			exit(-1); // invalid IP protocol
	}


	if (*bufferLength > FEC_LOWERLEVEL_MAX_PACKET_SIZE)
	{
		debugPrint("Segment is too big for UDP LSO, skipping... BufferLength: %d",*bufferLength);
		return;
	}

	bytesSent = sendto(udpEnv.socketDescriptor, buffer, *bufferLength, 0, (struct sockaddr *) workingSockAddr, sizeof(struct sockaddr));

	if (bytesSent < *bufferLength)
	{
		debugPrint("UDP: bytesSent < segmentLength, skipping..");
		return;
	}

	sleep_secs = udpEnv.sleepSecPerBit * (((sizeof(struct iphdr) + sizeof(struct udphdr)) + *bufferLength) * 8);

	// improve accuracy (computationTime subtracted)
	if (startTimestamp == 0)
		startTimestamp = getTimestamp();
	unsigned long endTimestamp = getTimestamp();
	unsigned int cycleTime = endTimestamp - startTimestamp;
	unsigned int computationTime = cycleTime - usecs;
	startTimestamp = getTimestamp();

	usecs = sleep_secs * 1000000.0;

	if (usecs>computationTime)
		usecs -= computationTime;

	if (usecs == 0)
	{
		usecs = 1;
	}

	usleep(usecs);
}
void receivePacketFromLowerProtocol	(char *buffer,int *bufferLength, void **udpProtocolData, unsigned int *udpProtocolDataSize)
{
	static struct sockaddr_in	fromAddr;
	socklen_t			fromSize;
	fromSize = sizeof(struct sockaddr_in);
	*bufferLength = recvfrom(udpEnv.socketDescriptor, buffer, FEC_LOWERLEVEL_MAX_PACKET_SIZE,	0, (struct sockaddr *) &fromAddr, &fromSize);
	*udpProtocolData= &fromAddr;
	*udpProtocolDataSize=sizeof(struct sockaddr_in);

	//todo How to manage an error during the reception of an UDP datagram?
	if(*bufferLength <= 0)
	{
		debugPrint("Can't acquire segment from UDP");
		exit(1);
	}
}

static unsigned long getTimestamp()
{
	struct timespec tms;
	unsigned long microseconds;
	if (clock_gettime(CLOCK_REALTIME,&tms))
	{
		return 0;
	}
	/* seconds, multiplied with 1 million */
	microseconds = tms.tv_sec * 1000000;
	/* Add full microseconds */
	microseconds += tms.tv_nsec/1000;
	/* round */
	if (tms.tv_nsec % 1000 >= 500)
		microseconds++;

	return microseconds;
}

