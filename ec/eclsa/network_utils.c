/*
network_utils.c

 Author: Andrea Lo Buglio (andrea.lobuglio@studio.unibo.it)
 	 	 Andrea Bisacchi (andreabisacchi@gmail.com)
 Project Supervisor: Carlo Caini (carlo.caini@unibo.it)

Copyright (c) 2016, Alma Mater Studiorum, University of Bologna
 All rights reserved.

This file implements parsHostnameAndPort, which from the address&port string extract
the IPv4 or IPv6 address and the port and insert them in the Internet format
into the addr structure

 */
#include <netdb.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <netinet/ip_icmp.h>
#include <netinet/udp.h>
#include <stdlib.h>
#include "network_utils.h"
#include "./adapters/protocol/eclsaProtocolAdapters.h"

static int buildAddressStruct(char *ip, char * portString, INET_struct *addr,uint16_t defaultPort);
static inline bool isLittleEndian();
static bool getInternetAddress2(char *hostname, INET_struct *addr);

int parseHostnameAndPort(char* hostnameAndPort, INET_struct *addr, uint16_t defaultPort) {
	char* hostnameAndPortCopy = strdup(hostnameAndPort);
	char* hostname;
	char* port;
	if (hostnameAndPort[0] == '[') {
		hostname = strtok(hostnameAndPortCopy, "[]");
		port = strtok(NULL, ":");
	} else {
		hostname = strtok(hostnameAndPortCopy, ":");
		port = strtok(NULL, ":");
	}

	int returned_value = buildAddressStruct(hostname, port, addr, defaultPort);
	free(hostnameAndPortCopy);
	return returned_value;
}

/***** PRIVATE METHODS *****/

static int buildAddressStruct(char *hostname, char * portString, INET_struct *addr, uint16_t defaultPort)
{
	unsigned int	portNumber;

	if (hostname == NULL || *hostname == '\0') {
		return -1;
	}

	// First figure out the IP address.  @ is local host.
	if (!getInternetAddress2(hostname, addr))
		return -1;

	//	Now pick out the port number, if requested.
	if (portString == NULL) { // No port number because argv[2]="" (argv[2] aka port)
		portNumber = defaultPort;
	} else {
		portNumber = atoi(portString); //from string to integer (all argv are strings)
	}

	if (portNumber < 1024 || portNumber > 65535)
		return -1; // port not acceptable (i4 > 65535 is not accepted) (i4 < 1024 requires root permissions)

	switch (addr->ai_family) {
	case AF_INET:
		addr->sock_address.ipv4.sin_port = (isLittleEndian() ? htons(portNumber) : portNumber);
		break;
	case AF_INET6:
		addr->sock_address.ipv6.sin6_port = (isLittleEndian() ? htons(portNumber) : portNumber);;
		break;
	default:
		return -1;
	}

	return 0;
}

static inline bool isLittleEndian() {
	int 		n = 1;
	return (*(char *)&n == 1);
}

static bool getInternetAddress2(char *hostname, INET_struct *addr) {
	if (strcmp(hostname, "@") == 0) {
		return false; //CCaini added because we do not want to accept the @ (used in ION)
	}
	if (strcmp(hostname, "0.0.0.0") == 0) {
		addr->ai_family = AF_INET; // ALB: using IPv4

		addr->sock_address.ipv4.sin_family = AF_INET; // ALB: using IPv4
		addr->sock_address.ipv4.sin_addr.s_addr = INADDR_ANY; // ALB: set IPv4 address to 0.0.0.0
		return true;
	}

	//part added by ALB to avoid warnings on gethostbyname
	struct addrinfo hints, *address_obtained;
	int status;

	memset(&hints, 0, sizeof(struct addrinfo));     // ALB: sets to 0 the firsts sizeof(struct addrinfo) bytes
	// in memory referenced by &hints
	hints.ai_family = AF_UNSPEC;    // ALB: to support both IPv4 and IPv6

	/* hostname is a string that can either be an IPv4 or IPv6 or a URI.
	 * If hostname is a URI getaddrinfo queries DNS.
	 * With hints.ai_family = AF_UNSPEC, it returns IPv4 address if available, otherwise IPv6 (if available).
	 * result is a pointer to the structure that contains the returned address
	 */
	status = getaddrinfo(hostname, NULL, &hints, &address_obtained); // from <netdb.h>
	if (status != 0) { // if an error occurred
		// putSysErrmsg("can't get address for host", hostname); // TODO: verify this function
		freeaddrinfo(address_obtained); // ALB: frees the memory located by getaddrinfo
		return false;
	}

	addr->ai_family = address_obtained->ai_family;
	switch (addr->ai_family) {
	case AF_INET:
		addr->sock_address.ipv4 = *((struct sockaddr_in*) address_obtained->ai_addr); // ALB: copying by reference IPv4 struct todo right?
		break;

	case AF_INET6:
		addr->sock_address.ipv6 = *((struct sockaddr_in6*) address_obtained->ai_addr); // ALB: copying by reference IPv4 struct todo right?
		break;

	default:
		freeaddrinfo(address_obtained); // ALB: frees the memory located by getaddrinfo
		return false;
	}

	// ALB: if we arrive to this part of code it means that we have found a valid IP address
	freeaddrinfo(address_obtained); // ALB: frees the memory located by getaddrinfo
	return true;
}
