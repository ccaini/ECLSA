# various mem settings (udp ...)
sysctl -w net.core.rmem_max=2621440
sysctl -w net.core.rmem_default=2621440
sysctl -w net.core.optmem_max=10240
sysctl -w net.ipv4.udp_mem="2621440 2621440 2621440"
sysctl -w net.ipv4.udp_rmem_min=2621440
sysctl -w net.ipv4.udp_wmem_min=4096

echo 50000 > /proc/sys/net/core/netdev_budget
echo 50000 > /proc/sys/net/core/netdev_max_backlog

#increase shared memory to increase sdr size
#sysctl kernel.shmall=557048070
#sysctl kernel.shmmax=557048070

# increase file open counter max
ulimit -n 40960
