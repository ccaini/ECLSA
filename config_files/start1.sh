#!/bin/bash

./kill_ECLSA.sh
./buffer_settings.sh

echo "Removing the ion.log and testfiles .."
rm -f ./ion.log
rm -f ./testfile*
rm -f ./libec.log
rm -f ./debug.txt
rm -f ./core
rm -f ./libec_*
cat /dev/null > outputECLSO.log
cat /dev/null > outputECLSI.log


ulimit -c unlimited
mkdir -p /tmp/sdr



ionstart -I vm1ECLSA.rc
bprecvfile ipn:1.2 &


exit 0
